/*
This programm is intended to manually control telescope position
through the standard guiding port via i2c interface which available
from standard WGA port.
Copyright (C) 2018  Alex Fomin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <QString>
#include <QMessageBox>
#include <QTypeInfo>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btConnect_clicked();

    void on_btDisconnect_clicked();

    void on_pbUp_clicked();

    void on_pbForward_clicked();

    void on_pbBack_clicked();

    void on_pbDown_clicked();

    void on_pbGo_clicked();

    void on_chbInverseRAdirection_toggled(bool checked);

    void on_chbInverseDecdirection_toggled(bool checked);

    void on_cbDrivingSpeed_currentIndexChanged(const QString &arg1);
    void on_pbMoveFinalToInitial_clicked();

    void onTimer();
    void on_ClockTimer();
private:
    Ui::MainWindow *ui;

    int vMovingForward;
    int vMovingBack;
    int vMovingUp;
    int vMovingDown;
    int vMovingTime;        // millisecond
    int vRAMovingTime;
    int vDecMovingTime;
    bool vRAMoving;
    bool vDecMoving;
    int vMovingSpeed;
    QString vMovingStatusString;

    QTimer  *vTimer, *vClockTimer;

// i2c Bus variables
    int     vFile;
    int     vAddress;
    u_int8_t vRegisterData;
    bool    vConnected;
//-----------------------

    QMessageBox vmsg;

    void setMovingCommands();
    int Prepare_I2C_file();
    void StartMoving();
    int TimerUpdate();
    void getRAMovingTime();
    void getDecMovingTime();
    void P2PMoving();
    void getMovingSpeed();
    void showMessage(QString vMessage);
};

#endif // MAINWINDOW_H
