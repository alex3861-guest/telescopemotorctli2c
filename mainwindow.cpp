/*
This programm is intended to manually control telescope position
through the standard guiding port via i2c interface which available
from standard WGA port.
Copyright (C) 2018  Alex Fomin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <unistd.h>

void fStop(int vFile1)
{
    close(vFile1);
}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    vTimer = new QTimer();
    vTimer->stop();
    vTimer->setInterval(1000);
    vTimer->setSingleShot(true);
    connect(vTimer, SIGNAL(timeout()), this, SLOT(onTimer()));
    vClockTimer = new QTimer();
    vClockTimer->stop();
    vClockTimer->setInterval(1000);
    vClockTimer->setSingleShot(false);
    connect(vClockTimer, SIGNAL(timeout()), this, SLOT(on_ClockTimer()));
    setMovingCommands();
    vRAMoving = false;
    vDecMoving = false;
    getMovingSpeed();
    vConnected = false;
    vRegisterData = 0xFF;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::getDecMovingTime()
{
    int vDecInitTime, vDecFinalTime, vSign;
    if(ui->chbInitialSouth->isChecked())
        vSign = -1;
    else
        vSign = 1;
    vDecInitTime = ui->sbInitDecd->value()*vSign*3600;
    vDecInitTime += ui->sbInitDecm->value()*vSign*60;
    vDecInitTime += ui->sbInitDecs->value()*vSign;

    if(ui->chbFinalSouth->isChecked())
        vSign = -1;
    else
        vSign = 1;
    vDecFinalTime = ui->sbFinalDecd->value()*vSign*3600;
    vDecFinalTime += ui->sbFinalDecm->value()*vSign*60;
    vDecFinalTime += ui->sbFinalDecs->value()*vSign;
    vDecMovingTime = ((vDecFinalTime - vDecInitTime)*1000)/15; // time in milliseconds
    vDecMovingTime /= vMovingSpeed;
}

void MainWindow::getRAMovingTime()
{
    int vHalfDay = 43200; //half of the day duration in seconds
    int vRAInitTime, vRAFinalTime;
    vRAInitTime = ui->sbInitRAh->value()*3600;
    vRAInitTime += ui->sbInitRAm->value()*60;
    vRAInitTime += ui->sbInitRAs->value();

    vRAFinalTime = ui->sbFinalRAh->value()*3600;
    vRAFinalTime += ui->sbFinalRAm->value()*60;
    vRAFinalTime += ui->sbFinalRAs->value();

    vRAMovingTime = (vRAFinalTime - vRAInitTime)*1000; //time in milliseconds
    if(vRAMovingTime < -vHalfDay)
    {
        vRAMovingTime += vHalfDay;
    }
    else
    {
        if(vRAMovingTime > vHalfDay)
        {
            vRAMovingTime -= vHalfDay;
        }
    }
    vRAMovingTime /= vMovingSpeed;
}

void MainWindow::getMovingSpeed()
{
    int i = ui->cbDrivingSpeed->currentIndex();
    switch(i)
    {
    case 0: vMovingSpeed = 0.5;
        break;
    case 1: vMovingSpeed = 2;
        break;
    case 2: vMovingSpeed = 16;
        break;
    }
}

void MainWindow::showMessage(QString vMessage)
{
    vmsg.setText(vMessage);
    vmsg.exec();
}

void MainWindow::setMovingCommands()
{
    if(ui->chbInverseRAdirection->isChecked())
    {
        vMovingForward = 0xFD;
        vMovingBack = 0xFE;
    }
    else
    {
        vMovingForward = 0xFE;
        vMovingBack = 0xFD;
    }
    if(ui->chbInverseDecdirection->isChecked())
    {
        vMovingUp = 0xF7;
        vMovingDown = 0xFB;
    }
    else
    {
        vMovingUp = 0xFB;
        vMovingDown = 0xF7;
    }
}

int MainWindow::Prepare_I2C_file()
{
    std::string vFileName;
    bool    vResult;
    vAddress = ui->leDevAddress->text().toInt(&vResult,16);
    if (!vResult || (vAddress > 127))
    {
        showMessage("Error!!! Device address is wrong.");
        return 1;
    }
    vFileName = ui->leBus->text().toStdString();
    vFile = open(vFileName.data(), O_RDWR);
    if (vFile<0)
    {
        showMessage("Error!!! Probably wrong i2c bus file name.");
        return 1;
    }
    ioctl(vFile,I2C_SLAVE,vAddress);
    vRegisterData = 0xFF;
    ssize_t vsize = write(vFile, &vRegisterData, 1);
    if(vsize!=1)
    {
        vConnected = false;
        ui->lbConnectionStatus->setText("Not connected");
        fStop(vFile);
        return 1;
    }
    else
    {
        vConnected = true;
        ui->lbConnectionStatus->setText("Connected");
        return 0;
    }
}

void MainWindow::P2PMoving()
{
        if(TimerUpdate()!=0)
            return;
        if(vRAMoving)
        {
            if(vRAMovingTime < 0)
            {
                vMovingTime = abs(vRAMovingTime);
                vRegisterData = vMovingBack;
            }
            else
            {
                vRegisterData = vMovingForward;
                vMovingTime = vRAMovingTime;
            }
            vRAMoving = false;
        }
        else
        {
            if(vDecMoving)
            {
                if(vDecMovingTime < 0)
                {
                    vMovingTime = abs(vDecMovingTime);
                    vRegisterData = vMovingDown;
                }
                else
                {
                    vRegisterData = vMovingUp;
                    vMovingTime = vDecMovingTime;
                }
                vDecMoving = false;
            }
            else
                return;
        }
        StartMoving();
}

void MainWindow::StartMoving()
{
    if(vTimer->isActive())
        return;
    vTimer->setInterval(vMovingTime);
    write(vFile, &vRegisterData, 1);
    vTimer->start();
    {
        if(vRegisterData == vMovingForward)
            vMovingStatusString = "Moving forward";
        if(vRegisterData == vMovingBack)
            vMovingStatusString = "Moving back";
        if(vRegisterData == vMovingUp)
            vMovingStatusString = "Moving up";
        if(vRegisterData == vMovingDown)
            vMovingStatusString = "Moving down";
        vClockTimer->start();
    }
}

int MainWindow::TimerUpdate()
{
     bool    vResult;
     vMovingTime = ui->leDrivingTime->text().toInt(&vResult,10);
     if (!vResult)
     {
         showMessage("Error!!! Period time is set wrong.");
         return 1;
     }
     return 0;
}

void MainWindow::on_btConnect_clicked()
{
    if(Prepare_I2C_file())
    {
        showMessage("Error!!! Failed to connect.");
    }
}

void MainWindow::on_btDisconnect_clicked()
{
    vTimer->stop();
    fStop(vFile);
    ui->lbConnectionStatus->setText("Not connected");
}


void MainWindow::on_pbUp_clicked()
{
    if(TimerUpdate()!=0)
        return;
    vRegisterData = vMovingUp;
    StartMoving();
}

void MainWindow::on_pbMoveFinalToInitial_clicked()
{
    ui->sbInitRAh->setValue(ui->sbFinalRAh->value());
    ui->sbInitRAm->setValue(ui->sbFinalRAm->value());
    ui->sbInitRAs->setValue(ui->sbFinalRAs->value());
    ui->sbInitDecd->setValue(ui->sbFinalDecd->value());
    ui->sbInitDecm->setValue(ui->sbFinalDecm->value());
    ui->sbInitDecs->setValue(ui->sbFinalDecs->value());
    if(ui->chbFinalSouth->isChecked())
        ui->chbInitialSouth->setChecked(true);
    else
        ui->chbInitialSouth->setChecked(false);
}

void MainWindow::on_pbForward_clicked()
{
    if(TimerUpdate()!=0)
        return;
    vRegisterData = vMovingForward;
    StartMoving();
}

void MainWindow::on_pbBack_clicked()
{
    if(TimerUpdate()!=0)
        return;
    vRegisterData = vMovingBack;
    StartMoving();
}

void MainWindow::on_pbDown_clicked()
{
    if(TimerUpdate()!=0)
        return;
    vRegisterData = vMovingDown;
    StartMoving();
}

void MainWindow::on_pbGo_clicked()
{
    getRAMovingTime();
    getDecMovingTime();
    vRAMoving = true;
    vDecMoving = true;
    P2PMoving();
}

void MainWindow::on_chbInverseRAdirection_toggled(bool checked)
{
    setMovingCommands();
}

void MainWindow::on_chbInverseDecdirection_toggled(bool checked)
{
    setMovingCommands();
}

void MainWindow::on_cbDrivingSpeed_currentIndexChanged(const QString &arg1)
{
    getMovingSpeed();
}

void MainWindow::onTimer()
{
    vRegisterData = 0xFF;
    write(vFile, &vRegisterData, 1);
    vClockTimer->stop();
    ui->statusBar->showMessage("Idle");
    P2PMoving();
}

void MainWindow::on_ClockTimer()
{
    int timeleft = vTimer->remainingTime()/1000;
    ui->statusBar->showMessage(vMovingStatusString+", "+QString().setNum(timeleft)+" sec left");
}

